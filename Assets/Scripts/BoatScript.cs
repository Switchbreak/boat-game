﻿using UnityEngine;
using System.Collections;

public class BoatScript : MonoBehaviour
{
	public Transform bullet;
	public float fireCooldown = 0.2f;
	public Vector3 thrustDirection = new Vector3( 0, 0, 5 );
	public Vector3 thrusterPosition = new Vector3( 1, 0, 0 );
	
	private float cooldownTimer;
	
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		CharacterMotion();
		WeaponInput();
	}

	void CharacterMotion ()
	{
//		Vector3 moveDirection = new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical"));
//		moveDirection = transform.TransformDirection (moveDirection);
//		moveDirection *= 0.5f;
//		
//		CharacterController controller = GetComponent<CharacterController>();
//		controller.Move (moveDirection);
//		
//		if( Input.GetKey( KeyCode.Z ) )
//		{
//			transform.Rotate( new Vector3( 0, 50 * Time.deltaTime, 0 ) );
//		}
		
		if( Input.GetKey( KeyCode.A ) )
		{
			// Add left thruster
			rigidbody.AddForceAtPosition( transform.forward, transform.Find("LeftThruster").position );
		}
		if( Input.GetKey( KeyCode.D ) )
		{
			// Add right thruster
			rigidbody.AddForceAtPosition( transform.forward, transform.Find("RightThruster").position );
		}
	}
	
	void WeaponInput()
	{
		if( cooldownTimer > 0 )
			cooldownTimer -= Time.deltaTime;
		else
		{
			if( Input.GetKey( KeyCode.E ) )
			{
				cooldownTimer = fireCooldown;
				Transform newBullet = (Transform)Instantiate( bullet, transform.position, Quaternion.identity );
				
				newBullet.GetComponent<BulletScript>().direction = new Vector3( transform.forward.z, transform.forward.y, -transform.forward.x );
			}
			if( Input.GetKey( KeyCode.Q ) )
			{
				cooldownTimer = fireCooldown;
				Transform newBullet = (Transform)Instantiate( bullet, transform.position, Quaternion.identity );
				
				newBullet.GetComponent<BulletScript>().direction = new Vector3( -transform.forward.z, transform.forward.y, transform.forward.x );
			}
		}
	}
}
