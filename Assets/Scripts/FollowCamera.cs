﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {
	
	public Transform target = null;
	public float smoothTime = 0.3f;
	private Vector3 velocity;
	public Vector2 offset = new Vector2( 0, 12 );
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if( target )
		{
			Vector3 destination = new Vector3( target.position.x - offset.x, transform.position.y, target.position.z - offset.y );
			transform.position = Vector3.SmoothDamp( transform.position, destination, ref velocity, smoothTime );
		}
	}
}
