﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour
{
	public float bulletSpeed = 50;
	public float aliveTime = 0.5f;
	public Vector3 direction = new Vector3( 1, 0, 0 );
	
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate( direction * bulletSpeed * Time.deltaTime );
		
		if( aliveTime > 0 )
			aliveTime -= Time.deltaTime;
		else
		{
			Destroy( gameObject );
		}
	}
}
